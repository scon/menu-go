package api

import (
	"bitbucket.org/menuone/menu-open-go/helper"
	"github.com/go-chi/chi"
	"net/http"
)

var manifest = map[string]string{
"version":     "0.1",
"name":        "Menu API",
"description": "Menu is a flexible Restaurant Management API.",
}

func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", apiGetAppManifestHandler)

	return r
}

// HealthCheck endpoint
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	helper.SendJson(w, http.StatusOK, manifest)
}

func apiGetAppManifestHandler(w http.ResponseWriter, r *http.Request) {
	helper.SendJson(w, http.StatusOK, manifest)
}
