package auth

import (
	"encoding/json"
	"log"
	"net/http"

	"bitbucket.org/menuone/menu-open-go/helper"
	userService "bitbucket.org/menuone/menu-open-go/user"
	"github.com/go-chi/chi"
)

// InitRouter initializes auth api routes.
func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Post("/login", apiLoginHandler)
	r.Post("/register", apiRegisterHandler)
	r.Post("/logout", apiLogoutHandler)
	return r
}

func apiLoginHandler(w http.ResponseWriter, r *http.Request) {
	creds := &Credentials{}
	err := json.NewDecoder(r.Body).Decode(creds)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	user, err := userService.ReadUserByEmail(creds.Email)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	match := CheckPasswordHash(creds.Password, user.Password)
	if !match {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	tokenStr, err := generateToken(user)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, Token{tokenStr})
}

func apiRegisterHandler(w http.ResponseWriter, r *http.Request) {
	creds := &Credentials{}
	err := json.NewDecoder(r.Body).Decode(creds)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	log.Println("Creadentials parsed from json.")

	hashedPassword, _ := HashPassword(creds.Password)
	var user userService.User
	user.Email = creds.Email
	user.Password = hashedPassword
	newUser, err := userService.CreateUser(&user)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	log.Println("User created.")

	helper.SendJson(w, http.StatusCreated, newUser)
}

func apiLogoutHandler(w http.ResponseWriter, r *http.Request) {
	// Add token to blacklist until token expiration time.
	helper.SendJson(w, http.StatusOK, nil)
}
