package auth

import (
	"time"

	"bitbucket.org/menuone/menu-open-go/conf"
	"bitbucket.org/menuone/menu-open-go/user"
	jwt "github.com/dgrijalva/jwt-go"
)

type Token struct {
	Token string `json:"token"`
}

func generateToken(user *user.User) (string, error) {
	token := jwt.New(jwt.GetSigningMethod("HS256"))
	claims := make(jwt.MapClaims)
	claims["sub"] = user.Model.ID
	claims["role"] = user.Role
	claims["email"] = user.Email
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	token.Claims = claims
	tokenstring, err := token.SignedString([]byte(conf.MySecret))
	if err != nil {
		return "", err
	}
	return tokenstring, nil
}
