package conf

import (
	"flag"
	"io/ioutil"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"

	"github.com/jinzhu/gorm"
)

var Db *gorm.DB
var Conf *Config
var Yaml *YamlData

const MySecret = "SenelisMegstaMociutesApvalumus"

func init() {
	Conf = &Config{}
}

type Config struct {
	HttpPort   int
	ConfigFile string
	LogPath    string
}

func GetConfig(configFile string) (*YamlData, error) {
	Yaml = &YamlData{}
	if configFile != "" {
		content, err := ReadFile(configFile)
		if err != nil {
			return nil, err
		}
		Yaml.Decode([]byte(content))
	}
	return Yaml, nil
}

func OverrideFlagsOnConfig(yaml *YamlData) {

	if f := flag.CommandLine.Lookup("httpPort"); f != nil {
		yaml.Port = ":" + strconv.Itoa(Conf.HttpPort)
	}

	if f := flag.CommandLine.Lookup("logPath"); f != nil {
		if len(f.Value.String()) > 0 {
			yaml.LogPath = Conf.LogPath
		}
	}
}

func ReadFile(fileName string) (string, error) {
	pwd, _ := os.Getwd()
	file, err := ioutil.ReadFile(pwd + "/" + fileName)
	if err != nil {
		log.Printf("%s file read error.  #%v\n", fileName, err)
	}
	return string(file), err
}

//func (c *Config) GetConfFromFile(fileName string) error {
//	pwd, _ := os.Getwd()
//	yamlFile, err := ioutil.ReadFile(pwd + "/" + fileName)
//	if err != nil {
//		log.Printf("%s file read error.  #%v\n", fileName, err)
//	}
//	return c.GetConfFromString(string(yamlFile))
//}

//func (c *Config) GetConfFromString(yamlString string) error {
//
//	err := yaml.Unmarshal([]byte(yamlString), c)
//	if err != nil {
//		log.Fatalf("%s parse error %v\n", yamlString, err)
//	}
//	return err
//}
