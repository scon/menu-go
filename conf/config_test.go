package conf

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetConfFromYamlString(t *testing.T) {
	const settings = `port: :8080
db:
  dialect: mysql
  user: menu
  password: menu
  dbname:   menu
  charset:  utf8
  server:   "tcp(localhost:3306)"
  params:   "charset=utf8&parseTime=True&loc=Local"
`
	assert := assert.New(t)

	conf := YamlData{}
	err := conf.Decode([]byte(settings))
	if err != nil {
		t.Error(err)
	}

	assert.Equal(conf.Port, ":8080")
	assert.Equal(conf.DB.Dialect, "mysql")
	assert.Equal(conf.DB.User, "menu")
	assert.Equal(conf.DB.Password, "menu")
	assert.Equal(conf.DB.DbName, "menu")
	assert.Equal(conf.DB.Charset, "utf8")
	assert.Equal(conf.DB.Server, "tcp(localhost:3306)")
	assert.Equal(conf.DB.Params, "charset=utf8&parseTime=True&loc=Local")
}
