package conf

import (
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
)

func SetupLogger() {
	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	log.SetFormatter(Formatter)

	filename := Yaml.LogPath

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		// Cannot open log file. Logging to stderr
		fmt.Println(err)
	} else {
		log.SetOutput(io.MultiWriter(os.Stdout, f))
	}

	handler := func() {
		log.Warn("exit")
	}
	log.RegisterExitHandler(handler)
}
