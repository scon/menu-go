package conf

import (
	"bytes"
	"fmt"

	"bitbucket.org/menuone/menu-open-go/helper"
	yaml "gopkg.in/yaml.v2"
)

// YAMLData is our common data struct
// with YAML struct tags
type YamlData struct {
	Port       string  `yaml:"port"`
	LogPath    string  `yaml:"logpath"`
	StaticPath string  `yaml:"staticpath"`
	DB         YamlDsn `yaml:"db"`
}

// MySQL db connection string
// [username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
// Mysql DSN (Data Source Name)
type YamlDsn struct {
	Dialect  string `yaml:"dialect"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Protocol string `yaml:"protocol"`
	Address  string `yaml:"address"`
	DbName   string `yaml:"dbname"`
	Params   string `yaml:"params"`
	Server   string `yaml:"server"`
	Charset  string `yaml:"charset"`
}

// ToYAML dumps the YAMLData struct to
// a YAML format bytes.Buffer
func (t *YamlData) ToYAML() (*bytes.Buffer, error) {
	d, err := yaml.Marshal(t)
	if err != nil {
		return nil, err
	}
	b := bytes.NewBuffer(d)
	return b, nil
}

// Decode will decode into TOMLData
func (t *YamlData) Decode(data []byte) error {
	return yaml.Unmarshal(data, t)
}

func (t YamlDsn) String() string {
	dbc := fmt.Sprintf("%s:%s@%s/%s", t.User, t.Password, t.Server, t.DbName)
	if helper.IsEmptyString(t.Params) {
		return dbc
	}
	dbc = dbc + "?" + t.Params
	return dbc
}
