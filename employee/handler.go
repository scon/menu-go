package employee

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/menuone/menu-open-go/helper"
	"github.com/go-chi/chi"
)

// InitRouter initializes employee api routes.
func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", apiEmployeesListGetHandler)
	r.Get("/{employeeID}", apiEmployeeByIdGetHandler)
	r.Post("/", apiEmployeePostHandler)
	r.Put("/{employeeID}", apiEmployeePutHandler)
	r.Delete("/{employeeID}", apiEmployeeDeleteHandler)
	r.Post("/{employeeID}/disable", apiEmployeeDisableHandler)
	r.Post("/{employeeID}/enable", apiEmployeeEnableHandler)
	return r
}

func apiEmployeePostHandler(w http.ResponseWriter, r *http.Request) {
	var employee Employee
	employee.Active = false
	newEmployee, err := createEmployee(&employee)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJson(w, http.StatusCreated, newEmployee)
}

func apiEmployeesListGetHandler(w http.ResponseWriter, r *http.Request) {
	employee, err := readAllEmployee()
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJson(w, http.StatusOK, employee)
}

func apiEmployeeByIdGetHandler(w http.ResponseWriter, r *http.Request) {

	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	employee, err := readEmployeeByID(employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJson(w, http.StatusOK, employee)
}

func apiEmployeePutHandler(w http.ResponseWriter, r *http.Request) {

	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var employee Employee
	err = json.NewDecoder(r.Body).Decode(&employee)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if employee.ID != employeeID {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	updatedEmployee, err := updateEmployee(&employee)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, updatedEmployee)
}

func apiEmployeeDeleteHandler(w http.ResponseWriter, r *http.Request) {
	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	deleted, err := deleteEmployee(employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)
}

func apiEmployeeDisableHandler(w http.ResponseWriter, r *http.Request) {
	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	employee, err := readEmployeeByID(employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	employee.Active = false

	updatedEmployee, err := updateEmployee(employee)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, updatedEmployee)
}

func apiEmployeeEnableHandler(w http.ResponseWriter, r *http.Request) {
	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	employee, err := readEmployeeByID(employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	employee.Active = true

	updatedEmployee, err := updateEmployee(employee)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, updatedEmployee)
}

func getEmployeeID(r *http.Request) (uint, error) {
	return getURLParam(r, "employeeID")
}

func getURLParam(r *http.Request, name string) (uint, error) {
	var value uint64
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		value, err = strconv.ParseUint(valueStr, 10, 64)
		if err != nil {
			return uint(value), err
		}
	}
	return uint(value), nil
}
