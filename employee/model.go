package employee

import (
	"bitbucket.org/menuone/menu-open-go/conf"
	"github.com/jinzhu/gorm"
)

// Employee represent Person and employee
type Employee struct {
	gorm.Model
	FirstName string
	LastName  string
	Email     string
	Phone     string
	Notes     string
	Duties    string
	UserID    uint
	Active    bool
}

func createEmployee(employee *Employee) (*Employee, error) {
	db := conf.Db.Create(&employee)
	if db.Error != nil {
		return nil, db.Error
	}
	return employee, nil
}

func updateEmployee(employee *Employee) (*Employee, error) {
	db := conf.Db.Model(employee).Update(employee)
	if db.Error != nil {
		return nil, db.Error
	}
	return employee, nil
}

func readAllEmployee() (*[]Employee, error) {
	var employees []Employee
	db := conf.Db.Find(&employees)
	if db.Error != nil {
		return nil, db.Error
	}
	return &employees, nil
}

func readEmployeeByID(employeeID uint) (*Employee, error) {
	var employee Employee
	db := conf.Db.Where("id = ?", employeeID).Find(&employee)
	if db.Error != nil {
		return nil, db.Error
	}
	return &employee, nil
}

func deleteEmployee(employeeID uint) (bool, error) {
	db := conf.Db.Where("id = ?", employeeID).Delete(&Employee{})
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}
