module bitbucket.org/menuone/menu-open-go

require (
	cloud.google.com/go v0.34.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20181014144952-4e0d7dc8888f // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/go-chi/chi v3.3.3+incompatible
	github.com/go-chi/jwtauth v3.3.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/gofrs/uuid v3.1.0+incompatible // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v0.0.0-20181116074157-8ec929ed50c3 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793
	golang.org/x/net v0.0.0-20181207154023-610586996380 // indirect
	google.golang.org/appengine v1.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
