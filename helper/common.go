package helper

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

func IsEmptyString(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}

func GetTokenFromUrl(r *http.Request) (string, error) {
	tokenFromUrl := r.URL.Query()["token"][0]
	if len(tokenFromUrl) > 1 {
		return tokenFromUrl, nil
	}

	return "", errors.New("Token not found")
}

func PanicErr(err error) {
	if err != nil {
		panic(err)
	}
}

func CheckErr(err error) {
	if err != nil {
		fmt.Println("Error: ", err)
	}
}
