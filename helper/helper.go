package helper

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"

	"github.com/pkg/errors"
)

const HeaderContentType = "Content-Type"
const MimeApplicationJSON = "application/json"

func GetUint64UriParam(r *http.Request, name string) (uint64, error) {
	var value uint64
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		value, err = strconv.ParseUint(valueStr, 10, 64)
		if err != nil {
			return uint64(value), err
		}
	}
	return uint64(value), nil
}

func GetUintUriParam(r *http.Request, name string) (uint, error) {
	v, err := GetUint64UriParam(r, name)
	return uint(v), err
}

func SendJson(w http.ResponseWriter, status int, obj interface{}) error {
	w.Header().Set(HeaderContentType, MimeApplicationJSON)
	jsonData, err := json.Marshal(obj)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Error encoding json response: %v", obj))
	}
	w.WriteHeader(status)
	_, err = w.Write(jsonData)
	return err
}

func SendJsonOk(w http.ResponseWriter, obj interface{}) error {
	return SendJson(w, http.StatusOK, obj)
}

func SendJsonError(w http.ResponseWriter, status int, obj interface{}) error {
	w.Header().Set(HeaderContentType, MimeApplicationJSON)
	jsonData, err := json.Marshal(obj)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Error encoding json response: %v", obj))
	}
	w.WriteHeader(status)
	_, err = w.Write(jsonData)
	return err
}
