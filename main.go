package main

import (
	"flag"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/menuone/menu-open-go/api"
	"bitbucket.org/menuone/menu-open-go/auth"
	"bitbucket.org/menuone/menu-open-go/employee"
	"bitbucket.org/menuone/menu-open-go/ordermanagement"
	"bitbucket.org/menuone/menu-open-go/payment"
	"bitbucket.org/menuone/menu-open-go/upload"
	"bitbucket.org/menuone/menu-open-go/ws"

	"bitbucket.org/menuone/menu-open-go/conf"
	"bitbucket.org/menuone/menu-open-go/menu"
	"bitbucket.org/menuone/menu-open-go/order"
	"bitbucket.org/menuone/menu-open-go/user"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

func processFlags() {
	flag.IntVar(&conf.Conf.HttpPort, "httpPort", 8080, "http listen port")
	flag.StringVar(&conf.Conf.ConfigFile, "config", "config.yml", "config file to user")
	flag.StringVar(&conf.Conf.LogPath, "logPath", "", "Log file path")
	flag.Parse()
}

func createRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Timeout(60 * time.Second))

	r.Group(func(r chi.Router) {
		r.Get("/health", api.HealthCheck)
		r.Mount("/uploads", upload.InitRouter())
		r.Mount("/api", api.InitRouter())
		r.Mount("/api/menu", menu.InitRouter())
		r.Mount("/api/user", user.InitUserRouter())
		r.Mount("/api/role", user.InitRoleRouter())
		r.Mount("/api/order", order.InitRouter())
		r.Mount("/api/ordermanage", ordermanagement.InitRouter())
		r.Mount("/api/employee", employee.InitRouter())
		r.Mount("/api/payment", payment.InitRouter())
		r.Mount("/api/auth", auth.InitRouter())
		r.Mount("/api/ws", ws.InitWsRouter())
		// r.Get("/pwa/*", static.MobileCustomerAppFilesHandler)
	})

	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, conf.Yaml.StaticPath)
	FileServer(r, "/", http.Dir(filesDir))
	return r
}

func AutoMigrate(db *gorm.DB) error {
	db = db.AutoMigrate(
		menu.Menu{},
		menu.MenuSection{},
		menu.MenuItem{},
		user.User{},
		user.Role{},
		order.Order{},
		order.OrderItem{},
		employee.Employee{},
		payment.Payment{},
	)
	return db.Error
}

func main() {
	processFlags()

	yaml, err := conf.GetConfig(conf.Conf.ConfigFile)
	if err != nil {
		log.Fatal(err)
	}

	conf.OverrideFlagsOnConfig(yaml)
	conf.SetupLogger()

	db := yaml.DB

	//conf.Db, err = gorm.Open(db.Dialect, "menu:menu@tcp(localhost:3306)/menu?charset=utf8&parseTime=True&loc=Local")
	conf.Db, err = gorm.Open(db.Dialect, db.String())
	if err != nil {
		log.Error(err)
		return
	}

	AutoMigrate(conf.Db)

	router := createRouter()

	log.Info("Server started at the port ", conf.Conf.HttpPort)
	err = http.ListenAndServe(conf.Yaml.Port, router)
	if err != nil {
		log.Errorf("server error: %s\n", err)
	}

}

func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		log.Fatal("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if _, err := os.Stat("./" + conf.Yaml.StaticPath + r.URL.Path); os.IsNotExist(err) {
			log.Info("static file handler. Not found 404 for %s redirecting to index.html ", r.URL)
			if strings.HasPrefix(r.URL.Path, "/pwa/") {
				http.ServeFile(w, r, "./"+conf.Yaml.StaticPath+"/pwa/app.html")
			} else if strings.HasPrefix(r.URL.Path, "/employee/") {
				http.ServeFile(w, r, "./"+conf.Yaml.StaticPath+"/employee/index.html")
			}
			return
		}
		fs.ServeHTTP(w, r)
	}))
}
