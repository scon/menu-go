package menu

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/menuone/menu-open-go/helper"
	"bitbucket.org/menuone/menu-open-go/upload"
	"github.com/go-chi/chi"
)

func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", apiMenuGetHandler)
	r.Get("/{menuId}/tree", apiMenuTreeGetHandler)
	r.Get("/{menuId}", apiMenuGetByIdHandler)
	r.Delete("/{menuId}", apiMenuDeleteHandler)
	r.Post("/", apiMenuPostHandler)
	r.Put("/{menuId}", apiMenuPutHandler)

	r.Get("/{menuId}/section", apiMenuSectionsHandler)
	r.Get("/{menuId}/section/{sectionId}", apiMenuSectionByIdHandler)
	r.Post("/{menuId}/section/{sectionId}/image", apiMenuSectionImageUploadHandler)
	r.Delete("/{menuId}/section/{sectionId}", apiMenuSectionDeleteHandler)
	r.Post("/{menuId}/section", apiMenuSectionPostHandler)
	r.Put("/{menuId}/section/{sectionId}", apiMenuSectionPutHandler)

	r.Get("/{menuId}/section/{sectionId}/item", apiMenuItemsHandler)
	r.Get("/{menuId}/section/{sectionId}/item/{menuItemId}", apiMenuItemByIdHandler)
	r.Post("/{menuId}/section/{sectionId}/item/{menuItemId}/image", apiMenuImageImageUploadHandler)
	r.Delete("/{menuId}/section/{sectionId}/item/{menuItemId}", apiMenuItemDeleteHandler)
	r.Post("/{menuId}/section/{sectionId}/item", apiMenuItemPostHandler)
	r.Put("/{menuId}/section/{sectionId}/item/{menuItemId}", apiMenuItemPutHandler)

	return r
}

//  REST API JSON handler used to delete specified menu.
//    DELETE /api/menu/{menuId}
func apiMenuDeleteHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}
	deleted, err := deleteMenu(uint(menuId))
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)
}

//  REST API JSON handler for new menu creation.
//    POST /api/menu/
func apiMenuPostHandler(w http.ResponseWriter, r *http.Request) {
	var menu Menu
	err := json.NewDecoder(r.Body).Decode(&menu)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	menu2, err := createMenu(&menu)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusCreated, menu2)
}

//-------------apiMenuSectionImageUploadHandler
func apiMenuSectionImageUploadHandler(w http.ResponseWriter, r *http.Request) {
	menuID, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	sectionID, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSection, err := readMenuSection(menuID, sectionID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	file, handle, err := r.FormFile("image")
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	defer file.Close()

	mimeType := handle.Header.Get("Content-Type")
	if mimeType != "image/jpeg" && mimeType != "image/png" {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	imagePath, err := upload.SaveFile(file, handle.Filename)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	menuSection.Image = imagePath

	menuSection, err = updateMenuSection(menuSection)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, menuSection)
}

func apiMenuImageImageUploadHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItemId, err := getMenuItemId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItem, err := readMenuSectionItem(menuId, menuSectionId, menuItemId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	file, handle, err := r.FormFile("image")
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	defer file.Close()

	mimeType := handle.Header.Get("Content-Type")
	if mimeType != "image/jpeg" && mimeType != "image/png" {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	imagePath, err := upload.SaveFile(file, handle.Filename)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	menuItem.Image = imagePath

	menuItem, err = updateMenuItem(menuItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, menuItem)
}

//-------------------------------

// REST API
//    PUT "/{menuId}"
func apiMenuPutHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var menu Menu
	err = json.NewDecoder(r.Body).Decode(&menu)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menu.ID != menuId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	menu2, err := updateMenu(&menu)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, menu2)
}

//  Return all menus
//    GET /api/menu
func apiMenuGetHandler(w http.ResponseWriter, r *http.Request) {
	menu, err := readAllMenu()
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJson(w, http.StatusOK, menu)
}

//  Return all menus in the json tree structure
//    GET /api/menu/{menuId}/tree
func apiMenuTreeGetHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menu, err := readMenuById(menuId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSections, err := readMenuSections(menuId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menu.Sections = *menuSections

	menuItems, err := readMenuAllItems(menuId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	for _, menuItem := range *menuItems {
		sectionIndex := getMenuSection(&menu.Sections, menuItem.MenuSectionID)
		if menu.Sections[sectionIndex].Items == nil {
			menu.Sections[sectionIndex].Items = make([]MenuItem, 0)
		}
		menu.Sections[sectionIndex].Items = append(menu.Sections[sectionIndex].Items, menuItem)
	}

	helper.SendJson(w, http.StatusOK, menu)
}

func getMenuSection(menuSections *[]MenuSection, sectionId uint) int {
	for i, v := range *menuSections {
		if v.ID == sectionId {
			return i
		}
	}
	return -1
}

//  Return all menus
//    GET /api/menu/{menuId}
func apiMenuGetByIdHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menu, err := readMenuById(menuId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	helper.SendJson(w, http.StatusOK, menu)
}

func getMenuId(r *http.Request) (uint, error) {
	return helper.GetUintUriParam(r, "menuId")
}

func getSectionId(r *http.Request) (uint, error) {
	return helper.GetUintUriParam(r, "sectionId")
}

func getMenuItemId(r *http.Request) (uint, error) {
	return helper.GetUintUriParam(r, "menuItemId")
}

// menu sections

// Returns a list of menu sections
//   GET /{menuId}/sections
func apiMenuSectionsHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSections, err := readMenuSections(menuId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	helper.SendJson(w, http.StatusOK, menuSections)
}

// Returns specified menu section
//   GET /{menuId}/section/{sectionId}
func apiMenuSectionByIdHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	sectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSection, err := readMenuSection(menuId, sectionId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	helper.SendJson(w, http.StatusOK, menuSection)
}

//  REST API JSON handler used to create a new menu section.
//    POST /api/menu/{menuId}/section
func apiMenuSectionPostHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	var menuSection MenuSection
	err = json.NewDecoder(r.Body).Decode(&menuSection)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	if menuSection.MenuID != menuId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	menuSection2, err := createMenuSection(&menuSection)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusCreated, menuSection2)
}

//  REST API JSON handler used to delete specified menu section.
//    DELETE /api/menu/{menuId}/section/{sectionId}
func apiMenuSectionDeleteHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}

	sectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}

	deleted, err := deleteMenuSection(menuId, sectionId)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)
}

// REST API used to update MenuSection
//   PUT /{menuId}/section/{sectionId}
func apiMenuSectionPutHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var menuSection MenuSection
	err = json.NewDecoder(r.Body).Decode(&menuSection)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menuSection.MenuID != menuId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menuSection.ID != menuSectionId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	menuSection2, err := updateMenuSection(&menuSection)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, menuSection2)
}

// Menu Item Handler functions

// Returns a list of menu items
//   GET /{menuId}/section/{sectionId}/item
func apiMenuItemsHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSections, err := readMenuSectionItems(menuId, menuSectionId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	helper.SendJson(w, http.StatusOK, menuSections)
}

// Returns a list of menu items
//   GET /{menuId}/section/{sectionId}/item/{menuItemId}
func apiMenuItemByIdHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItemId, err := getMenuItemId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItem, err := readMenuSectionItem(menuId, menuSectionId, menuItemId)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	helper.SendJson(w, http.StatusOK, menuItem)
}

//  REST API JSON handler used to delete specified menu item.
//    DELETE /api/menu/{menuId}/section/{sectionId}/item/{menuItemId}
func apiMenuItemDeleteHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}

	sectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}

	menuItemId, err := getMenuItemId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}

	deleted, err := deleteMenuItem(menuId, sectionId, menuItemId)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)
}

//  REST API JSON handler used to create a new menu item assigned to specified menu section.
//    POST /{menuId}/section/{sectionId}/item
func apiMenuItemPostHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	sectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	var menuItem MenuItem

	//var menuSection MenuSection
	err = json.NewDecoder(r.Body).Decode(&menuItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	if menuItem.MenuID != menuId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menuItem.MenuSectionID != sectionId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	menuItem2, err := createMenuItem(&menuItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusCreated, menuItem2)
}

// Http REST API endpoint used for menuItem update
//   PUT /{menuId}/section/{sectionId}/item/{menuItemId}
func apiMenuItemPutHandler(w http.ResponseWriter, r *http.Request) {
	menuId, err := getMenuId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuSectionId, err := getSectionId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItemId, err := getMenuItemId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var menuItem MenuItem
	err = json.NewDecoder(r.Body).Decode(&menuItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menuItem.MenuID != menuId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menuItem.MenuSectionID != menuSectionId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if menuItem.Model.ID != menuItemId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	menuSection2, err := updateMenuItem(&menuItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, menuSection2)
}
