package menu

import (
	"bitbucket.org/menuone/menu-open-go/conf"
	"github.com/jinzhu/gorm"
)

type Language struct {
	gorm.Model
	Code string
	Name string
}

type Menu struct {
	gorm.Model
	Name        string
	Description string
	Language    string
	Sections    []MenuSection
	Items       []MenuItem
}

type MenuSection struct {
	gorm.Model
	MenuID      uint
	Name        string
	Description string
	Image       string
	Items       []MenuItem
}

type MenuItem struct {
	gorm.Model
	MenuID          uint
	MenuSectionID   uint
	Name            string
	Description     string
	Price           int64 // price in cents same as used by stripe
	Image           string
	SuitableForDiet string
	Language        string
}

func readAllMenu() (*[]Menu, error) {
	var menus []Menu
	db := conf.Db.Find(&menus)
	if db.Error != nil {
		return nil, db.Error
	}
	return &menus, nil
}

func readAllMenuTree() (*[]Menu, error) {
	var menus []Menu
	var menuSection []MenuSection
	db := conf.Db.Find(&menus)
	if db.Error != nil {
		return nil, db.Error
	}
	db = conf.Db.Find(&menuSection)
	if db.Error != nil {
		return nil, db.Error
	}
	return &menus, nil
}

func readMenuById(menuId uint) (*Menu, error) {
	var menu Menu
	err := conf.Db.Where("id = ?", menuId).Find(&menu).Error
	if err != nil {
		return nil, err
	}
	return &menu, nil
}

func createMenu(menu *Menu) (*Menu, error) {
	db := conf.Db.Create(menu)
	if db.Error != nil {
		return nil, db.Error
	}
	return menu, nil
}

func updateMenu(menu *Menu) (*Menu, error) {
	db := conf.Db.Model(menu).Update(menu)
	if db.Error != nil {
		return nil, db.Error
	}
	return menu, nil
}

func deleteMenu(menuId uint) (bool, error) {
	if menuId <= 0 {
		return false, nil
	}

	menu := Menu{}
	menu.Model.ID = menuId
	db := conf.Db.Delete(&menu)
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}

func readMenuSections(menuId uint) (*[]MenuSection, error) {
	var menuSections []MenuSection
	//db := conf.Db.Where("menu_id = ?", menuId).Find(&menuSections)
	menu, err := readMenuById(menuId)
	if err != nil {
		return nil, err
	}

	db := conf.Db.Model(menu).Related(&menuSections) // Where("menu_id = ?", menuId).Find(&menuSections)
	if db.Error != nil {
		return nil, db.Error
	}
	return &menuSections, nil
}

func readMenuSection(menuId, sectionId uint) (*MenuSection, error) {
	var menuSection MenuSection
	db := conf.Db.First(&menuSection, sectionId)
	if db.Error != nil {
		return nil, db.Error
	}
	if menuSection.MenuID != menuId {
		return nil, gorm.ErrRecordNotFound
	}
	return &menuSection, nil
}

func readMenuItems(menuId, sectionId uint) (*[]MenuItem, error) {
	var menuItems []MenuItem
	menuItems = make([]MenuItem, 0)
	// db := conf.Db.Where("menu_section_id = ? AND menu_id = ?", sectionId, menuId).Find(&menuItems)
	db := conf.Db.Where(&MenuItem{MenuSectionID: sectionId, MenuID: menuId}).Find(&menuItems)
	if db.Error != nil {
		return nil, db.Error
	}
	return &menuItems, nil
}

func readMenuAllItems(menuId uint) (*[]MenuItem, error) {
	var menuItems []MenuItem
	menuItems = make([]MenuItem, 0)
	// db := conf.Db.Where("menu_section_id = ? AND menu_id = ?", sectionId, menuId).Find(&menuItems)
	db := conf.Db.Where(&MenuItem{MenuID: menuId}).Find(&menuItems)
	if db.Error != nil {
		return nil, db.Error
	}
	return &menuItems, nil
}

func updateMenuSection(menuSection *MenuSection) (*MenuSection, error) {
	db := conf.Db.Model(menuSection).Update(menuSection)
	if db.Error != nil {
		return nil, db.Error
	}
	return menuSection, nil
}

func createMenuSection(menuSection *MenuSection) (*MenuSection, error) {
	db := conf.Db.Create(menuSection)
	if db.Error != nil {
		return nil, db.Error
	}
	return menuSection, nil
}

func deleteMenuSection(menuId, sectionId uint) (bool, error) {
	if sectionId <= 0 {
		return false, nil
	}

	db := conf.Db.Where("menu_id = ? AND id = ?", menuId, sectionId).Delete(&MenuSection{})
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}

// Read Menu Section Items from DB.
func readMenuSectionItems(menuId, sectionId uint) (*[]MenuItem, error) {
	var menuItems *[]MenuItem
	//db := conf.Db.Where("menu_id = ?", menuId).Find(&menuSections)
	menuItems, err := readMenuItems(menuId, sectionId)
	if err != nil {
		return nil, err
	}
	return menuItems, nil
}

func readMenuSectionItem(menuId, sectionId, menuItemId uint) (*MenuItem, error) {
	var menuItem MenuItem

	db := conf.Db.Where("menu_id = ? AND menu_section_id = ? AND id = ?", menuId, sectionId, menuItemId).Find(&menuItem)
	if db.Error != nil {
		return nil, db.Error
	}
	return &menuItem, nil
}

func createMenuItem(menuItem *MenuItem) (*MenuItem, error) {
	db := conf.Db.Create(menuItem)
	if db.Error != nil {
		return nil, db.Error
	}
	return menuItem, nil
}

func updateMenuItem(menuItem *MenuItem) (*MenuItem, error) {
	db := conf.Db.Model(menuItem).Update(menuItem)
	if db.Error != nil {
		return nil, db.Error
	}
	return menuItem, nil
}

func deleteMenuItem(menuId, sectionId, menuItemId uint) (bool, error) {
	if menuItemId <= 0 {
		return false, nil
	}

	db := conf.Db.Where("menu_id = ? AND menu_section_id = ? AND id = ?", menuId, sectionId, menuItemId).Delete(&MenuItem{})
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}
