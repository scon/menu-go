package order

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/menuone/menu-open-go/helper"
	"bitbucket.org/menuone/menu-open-go/ws"
	"github.com/go-chi/chi"
	log "github.com/sirupsen/logrus"
)

type OrderAutoEmployeeSelector interface {
	AssignEmployee(order *Order) (*Order, error)
}

type AutoAssignToSuperWaitress struct {
	EmployeeID uint
}

type UnassignOrder struct{}

func (w *AutoAssignToSuperWaitress) AssignEmployee(order *Order) (*Order, error) {
	order.EmployeeID = w.EmployeeID
	return order, nil
}

func (w *UnassignOrder) AssignEmployee(order *Order) (*Order, error) {
	order.EmployeeID = 0
	return order, nil
}

func OrderEmployeeSelectorFactory() OrderAutoEmployeeSelector {
	return &AutoAssignToSuperWaitress{EmployeeID: 100}
}

// InitRouter initializes order api routes.
func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/{userID}", apiOrderGetHandler)
	r.Delete("/{userID}", apiOrderDeleteHandler)
	r.Post("/{userID}/{menuItemID}", apiOrderItemPostHandler)
	r.Put("/{userID}/{menuItemID}", apiOrderItemPutHandler)
	r.Delete("/{userID}/{menuItemID}", apiOrderItemDeleteHandler)
	r.Post("/{userID}/order", apiOrderStatusOrderHandler)
	r.Post("/{userID}/orderTaken", apiOrderStatusOrderTakenHandler)
	r.Post("/{userID}/payment", apiOrderStatusPaymentHandler)
	r.Post("/{userID}/deliver", apiOrderStatusDeliverHandler)
	r.Post("/{userID}/cancel", apiOrderStatusCancelHandler)
	r.Post("/{userID}/pickup", apiOrderStatusPickUpHandler)
	r.Post("/{userID}/table", apiOrderSetTableHandler)
	r.Get("/{userID}/callWaiter", apiOrderGetCallWaiter)
	r.Get("/{userID}/closeOrder", apiOrderGetCloseOrder)

	// Endpoint's for employee
	return r
}

func apiOrderGetHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var order Order
	order.UserID = userID

	orderFromDb, err := readOrderByUserId(userID)
	if err != nil {
		// superWaitress := AutoAssignToSuperWaitress{EmployeeID: 100}
		// superWaitress.AssignEmployee(&order)
		orderFromDb, err = createOrder(&order)
		if err != nil {
			helper.SendJsonError(w, http.StatusInternalServerError, err)
			return
		}
		helper.SendJson(w, http.StatusCreated, orderFromDb)
		return
	}

	helper.SendJson(w, http.StatusOK, orderFromDb)
}

func apiOrderDeleteHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	deleted, err := deleteOrder(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)
}

func apiOrderItemPostHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItemID, err := menuItemID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	orderID, err := getOrderIDByUserID(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var orderItem OrderItem
	err = json.NewDecoder(r.Body).Decode(&orderItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	orderItem.Sku = menuItemID
	orderItem.OrderID = orderID

	_, err = createOrderItem(&orderItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	order, err := readOrderByUserId(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	orderAmount := order.Amount + (orderItem.Price * int(orderItem.Qty))

	order, err = setOrderAmount(order, orderAmount)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	wsMsg := ws.Message{
		Type:    "order",
		UserID:  order.UserID,
		Payload: order,
	}
	err = ws.SendDataWithWebsocket(wsMsg)
	if err != nil {
		log.Error(err)
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderItemPutHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	menuItemID, err := menuItemID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	// orderID, err := getOrderIDByUserID(userID)
	// if err != nil {
	// 	helper.SendJsonError(w, http.StatusNotFound, err)
	// 	return
	// }

	var orderItem OrderItem
	err = json.NewDecoder(r.Body).Decode(&orderItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	// if orderID != orderItem.OrderID {
	// 	helper.SendJsonError(w, http.StatusBadRequest, err)
	// 	return
	// }

	if orderItem.Sku != menuItemID {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	_, err = updateOrderItem(&orderItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	order, err := calcOrderAmount(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	updatedOrder, err := updateOrder(order)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, updatedOrder)
}

func apiOrderItemDeleteHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	// menuItemID, err := menuItemID(r)
	// if err != nil {
	// 	helper.SendJsonError(w, http.StatusNotFound, err)
	// 	return
	// }

	// orderID, err := getOrderIDByUserID(userID)
	// if err != nil {
	// 	helper.SendJsonError(w, http.StatusNotFound, err)
	// 	return
	// }

	var orderItem OrderItem
	err = json.NewDecoder(r.Body).Decode(&orderItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	deleted, err := deleteOrderItem(&orderItem)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}

	// order, err := readOrderByUserId(userID)
	// if err != nil {
	// 	helper.SendJsonError(w, http.StatusNotFound, err)
	// 	return
	// }

	// orderAmount := order.Amount - (orderItem.Price * int(orderItem.Qty))

	// order, err = setOrderAmount(order, orderAmount)
	// if err != nil {
	// 	helper.SendJsonError(w, http.StatusNotFound, err)
	// 	return
	// }
	order, err := calcOrderAmount(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	updatedOrder, err := setOrderAmount(order, order.Amount)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, updatedOrder)
}

func getUserID(r *http.Request) (string, error) {
	return getURLParamString(r, "userID")
}

func menuItemID(r *http.Request) (uint, error) {
	return getURLParam(r, "menuItemID")
}

func getURLParam(r *http.Request, name string) (uint, error) {
	var value uint64
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		value, err = strconv.ParseUint(valueStr, 10, 64)
		if err != nil {
			return uint(value), err
		}
	}
	return uint(value), nil
}

func getURLParamString(r *http.Request, name string) (string, error) {
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		return valueStr, nil

	}
	return "", err
}

func apiOrderStatusOrderHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	_, err = setOrderStatus(userID, OrderStatusOrder) //find better way for get full order with items.
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	fullOrder, err := readOrderByUserId(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	err = PushOrderForWaiter()
	if err != nil {
		log.Error(err)
	}

	helper.SendJson(w, http.StatusOK, fullOrder)
}

func apiOrderStatusOrderTakenHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := setOrderStatus(userID, OrderStatusTaken)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderStatusPaymentHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := setOrderStatus(userID, OrderStatusPayment)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderStatusDeliverHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := setOrderStatus(userID, OrderStatusDeliver)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderStatusCancelHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := setOrderStatus(userID, OrderStatusCancel)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderStatusPickUpHandler(w http.ResponseWriter, r *http.Request) {

	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := setOrderStatus(userID, OrderStatusPickUp)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderGetCallWaiter(w http.ResponseWriter, r *http.Request) {
	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := readOrderByUserId(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err = UpdateCallWaiterStatus(order, true)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	err = PushOrderForWaiter()
	if err != nil {
		log.Error(err)
	}

	helper.SendJson(w, http.StatusOK, order)
}

func apiOrderGetCloseOrder(w http.ResponseWriter, r *http.Request) {
	userID, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err := readOrderByUserId(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	order, err = setOrderStatus(userID, OrderStatusClosed)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func calcOrderAmount(userID string) (*Order, error) {
	order, err := readOrderByUserId(userID)
	if err != nil {
		return nil, err
	}
	var orderAmount int
	for _, item := range order.OrderItems {
		orderAmount += int(item.Qty) * item.Price
	}
	order.Amount = orderAmount
	return order, nil
}

func apiOrderSetTableHandler(w http.ResponseWriter, r *http.Request) {
	_, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var order Order
	err = json.NewDecoder(r.Body).Decode(&order)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	order2, err := UpdateOrderTable(&order, order.Table)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order2)
}
