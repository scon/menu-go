package order

import (
	"bitbucket.org/menuone/menu-open-go/conf"
	"bitbucket.org/menuone/menu-open-go/ws"
	"github.com/jinzhu/gorm"
)

// Order structure
type Order struct {
	gorm.Model
	UserID        string
	CustomerName  string
	OrderNumber   int
	OrderDate     string
	Amount        int
	Status        string
	PaymentStatus string
	EmployeeID    uint
	CallWaiter    bool
	Table         uint
	OrderItems    []OrderItem
}

type OrderItem struct {
	gorm.Model
	OrderID     uint
	Sku         uint
	Code        string
	Name        string
	Qty         uint
	Price       int
	Description string
	Image       string
	Status      string
}

// Order statuses
const (
	OrderStatusOrder   string = "order"
	OrderStatusTaken   string = "orderTaken"
	OrderStatusPickUp  string = "pickup"
	OrderStatusDeliver string = "deliver"
	OrderStatusPayment string = "payment"
	OrderStatusCancel  string = "cancel"
	OrderStatusClosed  string = "closed"
)

func createOrder(order *Order) (*Order, error) {
	db := conf.Db.Create(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}

func updateOrder(order *Order) (*Order, error) {
	db := conf.Db.Model(order).Update(order)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}

func readOrderByUserId(userID string) (*Order, error) {
	var order Order
	db := conf.Db.Where("user_id = ? AND status <> ?", userID, OrderStatusClosed).Find(&order)
	if db.Error != nil {
		return nil, db.Error
	}

	var OrderItems []OrderItem
	db = conf.Db.Model(order).Related(&OrderItems)
	if db.Error != nil {
		return nil, db.Error
	}

	order.OrderItems = OrderItems

	return &order, nil
}

func deleteOrder(userID string) (bool, error) {
	db := conf.Db.Where("user_id = ?", userID).Delete(&Order{})
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}

func createOrderItem(orderItem *OrderItem) (*OrderItem, error) {
	db := conf.Db.Create(&orderItem)
	if db.Error != nil {
		return nil, db.Error
	}
	return orderItem, nil
}

func deleteOrderItem(orderItem *OrderItem) (bool, error) {
	db := conf.Db.Delete(&orderItem)
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}

func getOrderIDByUserID(userID string) (uint, error) {
	var order Order
	db := conf.Db.Where("user_id = ?", userID).Find(&order)
	if db.Error != nil {
		return 0, db.Error
	}
	return order.ID, nil
}

func setOrderStatus(userID string, orderStatus string) (*Order, error) {
	var order Order
	if orderStatus == OrderStatusOrder {
		i := OrderEmployeeSelectorFactory()
		i.AssignEmployee(&order)
	}
	db := conf.Db.Model(&order).Where("user_id = ?", userID).Updates(Order{Status: orderStatus, EmployeeID: order.EmployeeID}).First(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return &order, nil
}

func updateOrderItem(orderItem *OrderItem) (*OrderItem, error) {
	db := conf.Db.Model(orderItem).Updates(orderItem).First(orderItem)
	if db.Error != nil {
		return nil, db.Error
	}
	return orderItem, nil
}

func setOrderAmount(order *Order, amount int) (*Order, error) {
	db := conf.Db.Model(order).Update("amount", amount)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}

func UpdateCallWaiterStatus(order *Order, callWaiter bool) (*Order, error) {
	db := conf.Db.Model(order).Update("call_waiter", callWaiter)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}

func UpdateOrderTable(order *Order, table uint) (*Order, error) {
	db := conf.Db.Model(order).Update("table", table)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}

func readOrders() (*[]Order, error) {
	var order []Order
	db := conf.Db.Preload("OrderItems").Find(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return &order, nil
}

func PushOrderForWaiter() error {
	orders, err := readOrders()
	if err != nil {
		return err
	}

	wsMsg := ws.Message{
		Type:    "orders",
		UserID:  "waiter",
		Payload: orders,
	}
	err = ws.SendDataWithWebsocket(wsMsg)
	if err != nil {
		return err
	}
	return nil
}
