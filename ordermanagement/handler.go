package ordermanagement

import (
	"encoding/json"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/menuone/menu-open-go/helper"
	"bitbucket.org/menuone/menu-open-go/order"
	"bitbucket.org/menuone/menu-open-go/token"
	"bitbucket.org/menuone/menu-open-go/ws"
	"github.com/go-chi/chi"
)

// InitRouter initializes order management api routes.
func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Use(token.Authenticator)
	r.Get("/", apiOrdersGetHandler)
	r.Post("/take/{employeeID}", apiTakeOrder)
	r.Get("/{employeeID}", apiOrdersGetEmployeeActiveOrders)
	r.Get("/order/{orderID}", apiOrderGetByIdHandler)
	r.Post("/refuse/{employeeID}", apitOrderRefuseOrder)
	r.Get("/call/{orderID}", apiOrderGetCallWaiterHandler)
	return r
}

func apiOrderGetCallWaiterHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := getOrderID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	order1, err := readOrderById(orderID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	order1, err = order.UpdateCallWaiterStatus(order1, false)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	wsMsg := ws.Message{
		Type:    "order",
		UserID:  order1.UserID,
		Payload: order1,
	}
	err = ws.SendDataWithWebsocket(wsMsg)
	if err != nil {
		log.Error(err)
	}

	orders, err := readOrders()
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, orders)
}

func apiOrdersGetHandler(w http.ResponseWriter, r *http.Request) {
	orders, err := readOrders()
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJson(w, http.StatusOK, orders)
}

func apiTakeOrder(w http.ResponseWriter, r *http.Request) {

	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	var order order.Order
	err = json.NewDecoder(r.Body).Decode(&order)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	order2, err := takeOrder(&order, employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	wsMsg := ws.Message{
		Type:    "order",
		UserID:  order2.UserID,
		Payload: order2,
	}
	err = ws.SendDataWithWebsocket(wsMsg)
	if err != nil {
		log.Error(err)
	}

	helper.SendJson(w, http.StatusOK, order2)
}

func apiOrdersGetEmployeeActiveOrders(w http.ResponseWriter, r *http.Request) {
	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	orders, err := readEmployeeOrders(employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJson(w, http.StatusOK, orders)
}

func apitOrderRefuseOrder(w http.ResponseWriter, r *http.Request) {
	employeeID, err := getEmployeeID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	var order order.Order
	err = json.NewDecoder(r.Body).Decode(&order)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	_, err = refuseOrder(&order, employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	orders, err := readEmployeeOrders(employeeID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, orders)
}

func apiOrderGetByIdHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := getOrderID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	order, err := readOrderById(orderID)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, order)
}

func getOrderID(r *http.Request) (uint, error) {
	return getURLParam(r, "orderID")
}

func getEmployeeID(r *http.Request) (uint, error) {
	return getURLParam(r, "employeeID")
}

func getURLParam(r *http.Request, name string) (uint, error) {
	var value uint64
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		value, err = strconv.ParseUint(valueStr, 10, 64)
		if err != nil {
			return uint(value), err
		}
	}
	return uint(value), nil
}
