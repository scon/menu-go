package ordermanagement

import (
	"bitbucket.org/menuone/menu-open-go/conf"
	"bitbucket.org/menuone/menu-open-go/order"
)

// loads all orders with items.
func readOrders() (*[]order.Order, error) {
	var order []order.Order
	db := conf.Db.Preload("OrderItems").Find(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return &order, nil
}

func takeOrder(orderIn *order.Order, employeeID uint) (*order.Order, error) {
	db := conf.Db.Model(&orderIn).Updates(order.Order{EmployeeID: employeeID, Status: "orderTaken"})
	if db.Error != nil {
		return nil, db.Error
	}
	return orderIn, nil
}

func refuseOrder(order *order.Order, employeeID uint) (*order.Order, error) {
	db := conf.Db.Model(&order).Update("EmployeeID", 0)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}

func readEmployeeOrders(employeeID uint) (*[]order.Order, error) {
	var order []order.Order
	db := conf.Db.Preload("OrderItems").Where("employee_id = ?", employeeID).Find(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return &order, nil
}

func readOrderById(orderID uint) (*order.Order, error) {
	var order order.Order
	db := conf.Db.Preload("OrderItems").Where("id = ?", orderID).First(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return &order, nil
}
