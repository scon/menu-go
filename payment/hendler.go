package payment

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/menuone/menu-open-go/order"

	"bitbucket.org/menuone/menu-open-go/helper"
	"github.com/go-chi/chi"
)

// InitRouter initializes payment api routes.
func InitRouter() http.Handler {
	r := chi.NewRouter()
	r.Post("/{orderID}", apiPaymentGetHandler)
	return r
}

func apiPaymentGetHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := getOrderID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var payment Payment
	err = json.NewDecoder(r.Body).Decode(&payment)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if payment.OrderID != orderID {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	order1, err := readOrderById(orderID)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	order1.Status = order.OrderStatusPayment

	_, err = updateOrder(order1)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	payment2, err := registerPayment(&payment)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	helper.SendJson(w, http.StatusOK, payment2)
}

func getOrderID(r *http.Request) (uint, error) {
	return getURLParam(r, "orderID")
}

func getURLParam(r *http.Request, name string) (uint, error) {
	var value uint64
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		value, err = strconv.ParseUint(valueStr, 10, 64)
		if err != nil {
			return uint(value), err
		}
	}
	return uint(value), nil
}
