package payment

import (
	"bitbucket.org/menuone/menu-open-go/conf"
	"bitbucket.org/menuone/menu-open-go/order"
	"github.com/jinzhu/gorm"
)

type Payment struct {
	gorm.Model
	OrderID       uint
	Amount        uint
	Change        uint
	Payment       uint
	PaymentMethod string
}

func registerPayment(payment *Payment) (*Payment, error) {
	db := conf.Db.Create(&payment)
	if db.Error != nil {
		return nil, db.Error
	}
	return payment, nil
}

func readOrderById(orderID uint) (*order.Order, error) {
	var order order.Order
	db := conf.Db.Preload("OrderItems").Where("id = ?", orderID).First(&order)
	if db.Error != nil {
		return nil, db.Error
	}
	return &order, nil
}

func updateOrder(order *order.Order) (*order.Order, error) {
	db := conf.Db.Model(order).Update(order)
	if db.Error != nil {
		return nil, db.Error
	}
	return order, nil
}
