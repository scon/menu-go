package token

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/menuone/menu-open-go/conf"
	"bitbucket.org/menuone/menu-open-go/helper"
	jwt "github.com/dgrijalva/jwt-go"
)

type contextKey string

var ContextKeyClaims contextKey = "claims"

func Authenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := strings.Split(r.Header.Get("Authorization"), " ")[1]
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			// Don't forget to validate the alg is what you expect:
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			var hmacSampleSecret = []byte(conf.MySecret)
			return hmacSampleSecret, nil
		})
		ctx := r.Context()

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			// log.Println(claims["sub"])
			ctx = context.WithValue(ctx, ContextKeyClaims, claims)
		} else {
			log.Println(err)
			helper.SendJsonError(w, http.StatusUnauthorized, err)
			ctx.Done()
			return
		}
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
