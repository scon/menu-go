package upload

import (
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/go-chi/chi"
)

func InitRouter() http.Handler {

	createUploadsDirIfNotExist()

	r := chi.NewRouter()

	r.Get("/*path", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./"+r.URL.Path)
	})

	return r
}

func SaveFile(file io.Reader, filename string) (string, error) {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	filePath := "./uploads/" + filename

	err = ioutil.WriteFile(filePath, data, 0666)
	if err != nil {
		return "", err
	}
	return filePath[1:], nil
}

func createUploadsDirIfNotExist() {
	newpath := filepath.Join(".", "uploads")
	os.MkdirAll(newpath, os.ModePerm)
}
