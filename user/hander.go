package user

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/menuone/menu-open-go/api"
	"bitbucket.org/menuone/menu-open-go/helper"
	"bitbucket.org/menuone/menu-open-go/token"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
)

type Response struct {
	Code   int         `json:"code,omitempty"`
	Result interface{} `json:"result,omitempty"`
	Meta   interface{} `json:"meta,omitempty"`
}

func InitUserRouter() http.Handler {
	r := chi.NewRouter()
	r.Use(token.Authenticator)
	r.Get("/", apiUsersGetHandler)
	r.Get("/{userId}", apiUserGetByIdHandler)
	r.Delete("/{userId}", apiUserDeleteHandler)
	r.Get("/me", apiUserMeHandler)
	r.Post("/", apiUserPostHandler)
	r.Put("/", apiUserPutHandler)
	return r
}

func InitRoleRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", apiRolesGetHandler)
	r.Get("/{roleId}", apiRoleGetByIdHandler)
	r.Delete("/{roleId}", apiRoleDeleteHandler)
	r.Post("/", apiRolePostHandler)
	r.Put("/", apiRolePutHandler)
	return r
}

// REST API JSON Handler to return current user based on JWT token
//   GET /api/user/me
func apiUserMeHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	claims := ctx.Value(token.ContextKeyClaims).(jwt.MapClaims)
	userID := uint(claims["sub"].(float64))
	userInfo, err := readUserById(userID)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}
	helper.SendJson(w, http.StatusOK, userInfo)
}

// REST API JSON Handler to return all users
//   GET /api/user/{userId}
func apiUsersGetHandler(w http.ResponseWriter, r *http.Request) {
	offset, limit, err := api.PaginateMinimal(w, r)
	users, err := readAllUsers(offset, limit)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJsonOk(w, users)
}

// REST API JSON Handler to delete user by userId
//   DELETE /api/user/{userId}
func apiUserDeleteHandler(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}
	deleted, err := deleteUser(userId)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)
}

// REST API JSON Handler to return user by userId
//   GET /api/user/{userId}
func apiUserGetByIdHandler(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, "Resource not found.")
		return
	}
	user, err := readUserById(userId)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJsonOk(w, user)
}

func getUserId(r *http.Request) (uint, error) {
	return helper.GetUintUriParam(r, "userId")
}

// REST API JSON handler to add new User
//  POST /api/user/
//    post body: Json object of user.User struct
//
func apiUserPostHandler(w http.ResponseWriter, r *http.Request) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	newUser, err := CreateUser(&user)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, newUser)
}

// REST API
//    PUT "/api/user/{userId}"
func apiUserPutHandler(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var user User
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if user.ID != userId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	user2, err := updateUser(&user)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, user2)
}

// Role Handlers

func getRoleId(r *http.Request) (uint, error) {
	return helper.GetUintUriParam(r, "roleId")
}

func apiRolesGetHandler(w http.ResponseWriter, r *http.Request) {
	offset, limit, err := api.PaginateMinimal(w, r)
	roles, err := readAllRoles(offset, limit)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJsonOk(w, roles)
}

func apiRoleDeleteHandler(w http.ResponseWriter, r *http.Request) {
	roleId, err := getRoleId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}
	deleted, err := deleteRole(roleId)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	if !deleted {
		helper.SendJsonError(w, http.StatusNoContent, nil)
		return
	}
	helper.SendJson(w, http.StatusOK, nil)

}

// REST API
//   GET /api/role/{roleId}
func apiRoleGetByIdHandler(w http.ResponseWriter, r *http.Request) {
	roleId, err := getRoleId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, "Resource not found.")
		return
	}

	roles, err := readRoleById(roleId)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}
	helper.SendJsonOk(w, roles)
}

// REST API JSON handler to add new Role
//  POST /api/role/
//    post body: Json object of user.Role struct
//
func apiRolePostHandler(w http.ResponseWriter, r *http.Request) {
	var role Role
	err := json.NewDecoder(r.Body).Decode(&role)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}
	newRole, err := createRole(&role)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, newRole)
}

// REST API
//    PUT "/api/role/{roleId}"
func apiRolePutHandler(w http.ResponseWriter, r *http.Request) {
	roleId, err := getRoleId(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, err)
		return
	}

	var role Role
	err = json.NewDecoder(r.Body).Decode(&role)
	if err != nil {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	if role.ID != roleId {
		helper.SendJsonError(w, http.StatusBadRequest, err)
		return
	}

	roleUpdated, err := updateRole(&role)
	if err != nil {
		helper.SendJsonError(w, http.StatusInternalServerError, err)
		return
	}

	helper.SendJson(w, http.StatusOK, roleUpdated)
}
