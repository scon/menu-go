package user

import (
	"bitbucket.org/menuone/menu-open-go/conf"
	"github.com/jinzhu/gorm"
)

type LoginForm struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

// User structure
type User struct {
	gorm.Model
	GUID      string
	Name      string
	FirstName string
	LastName  string
	Email     string
	Phone     string
	Notes     string
	Active    string
	Role      string
	Password  string
}

// Role structure represent users roles
type Role struct {
	gorm.Model
	Code        string
	Name        string
	Description string
}

// Roles structure represent relationship between user and role
type Roles struct {
	gorm.Model
	UserID uint
	RoleID uint
}

func readAllRoles(offset, limit uint64) (*[]Role, error) {
	roles := []Role{}
	err := conf.Db.Offset(offset).Limit(limit).Find(&roles).Error
	if err != nil {
		return nil, err
	}
	return &roles, nil
}

func readRoleById(roleId uint) (*Role, error) {
	var role Role
	err := conf.Db.Where("id = ?", roleId).Find(&role).Error
	if err != nil {
		return nil, err
	}
	return &role, nil
}

func deleteRole(roleId uint) (bool, error) {
	if roleId <= 0 {
		return false, nil
	}

	role := Role{}
	role.Model.ID = roleId
	db := conf.Db.Delete(&role)
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}

func createRole(role *Role) (*Role, error) {
	err := conf.Db.Create(role).Error
	if err != nil {
		return nil, err
	}
	return role, nil
}

func updateRole(role *Role) (*Role, error) {
	err := conf.Db.Model(role).Update(role).Error
	if err != nil {
		return nil, err
	}
	return role, nil
}

func readAllUsers(offset, limit uint64) (*[]User, error) {
	users := []User{}
	err := conf.Db.Offset(offset).Limit(limit).Find(&users).Error
	if err != nil {
		return nil, err
	}
	return &users, nil
}

func readUserById(userId uint) (*User, error) {
	var user User
	err := conf.Db.Where("id = ?", userId).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func ReadUserByEmail(email string) (*User, error) {
	var user User
	err := conf.Db.Where("email = ?", email).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func deleteUser(userId uint) (bool, error) {
	if userId <= 0 {
		return false, nil
	}

	user := User{}
	user.Model.ID = userId
	db := conf.Db.Delete(&user)
	if db.Error != nil {
		return false, db.Error
	}
	deleted := db.RowsAffected > 0
	return deleted, nil
}

func CreateUser(user *User) (*User, error) {
	err := conf.Db.Create(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func updateUser(user *User) (*User, error) {
	err := conf.Db.Model(user).Update(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
