package ws

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/menuone/menu-open-go/helper"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
)

var clients = make(map[string]*websocket.Conn) // connected clients
// var broadcast = make(chan Message)           // broadcast channel

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Message struct {
	Type    string      `json:"type"`
	UserID  string      `json:'userID'`
	Payload interface{} `json:"payload"`
}

func InitWsRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/{userId}", handleWsConnection)
	return r
}

func handleWsConnection(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
		return
	}

	userId, err := getUserID(r)
	if err != nil {
		helper.SendJsonError(w, http.StatusNotFound, "Resource not found.")
		return
	}

	// Register our new client
	clients[userId] = ws
}

func SendDataWithWebsocket(msg Message) error {
	client, ok := clients[msg.UserID]
	if !ok {
		return fmt.Errorf("not found with userID: %s", msg.UserID)
	}
	err := client.WriteJSON(msg)
	if err != nil {
		client.Close()
		delete(clients, msg.UserID)
		return err
	}
	return nil
}

func getUserID(r *http.Request) (string, error) {
	return getURLParamString(r, "userId")
}

func getURLParamString(r *http.Request, name string) (string, error) {
	var err error
	if valueStr := chi.URLParam(r, name); valueStr != "" {
		return valueStr, nil
	}
	return "", err
}
